package com.example.demo.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.service.Springtaskservice;
import com.example.demo.service.TransactionType;

public class Sprintaskcontroller {
@Autowired
	    private Springtaskservice springtaskservice;
	
	    @PostMapping("/")
	    public ResponseEntity<String> addTransaction(@RequestParam BigDecimal amount, @RequestParam TransactionType transactionType) {
	    	springtaskservice.addTransaction(amount, transactionType);
	        return ResponseEntity.ok("Transaction added successfully.");
	    }
	
	    @GetMapping("/")
	    public ResponseEntity<Map<Object, Long>> getTransactionFrequency() {
	        Map<Object, Long> transactionFrequency = springtaskservice.getTransactionFrequency();
	        return ResponseEntity.ok(transactionFrequency);
	    }
	
	    @GetMapping("/")
	    public ResponseEntity<List<Date>> getNegativeBalanceDates() {
	        List<Date> negativeBalanceDates = springtaskservice.getNegativeBalanceDates();
	        return ResponseEntity.ok(negativeBalanceDates);
	    }

}
