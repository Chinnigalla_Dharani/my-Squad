package com.example.demo.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Id;
import javax.transaction.Transaction;

import com.example.demo.service.TransactionType;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;

public class Springtaskentity {
@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private Transaction Transactiontype;
	private BigDecimal amount;
	private String frequency;
	private Date date;
	public TransactionType getTransactiontype;
	public Springtaskentity() {
		super();
	}

	public Transaction getTransactiontype() {
		return Transactiontype;
	}

	public void setTransactiontype(Transaction transactiontype) {
		Transactiontype = transactiontype;
	}

	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Springtaskentity(Transaction type, BigDecimal amount, String frequency, Date date) {
		super();
		this.Transactiontype = type;
		this.amount = amount;
		this.frequency = frequency;
		this.date = date;
	}

}
