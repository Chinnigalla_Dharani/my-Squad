package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Springtaskentity;

public interface Springtaskrepository extends JpaRepository<Springtaskentity,Integer>{

}
