package com.example.demo.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transaction;

import com.example.demo.entity.Springtaskentity;

public class Springtaskservice {
	private List<Springtaskentity> springtaskentity = new ArrayList<>();
	 
    public void addTransaction(BigDecimal amount, TransactionType transactionType) {
    	Springtaskentity springtaskentity = new Springtaskentity();
    	((List<Springtaskentity>) springtaskentity).add(springtaskentity);
    }
 
    public Map<Object, Long> getTransactionFrequency() {
        return springtaskentity.stream()
                .collect(
                        Collectors.groupingBy(
                        		springtaskentity -> new SimpleDateFormat("yyyy-MM-dd").format(((springtaskentity) ).  getDate()),
                                Collectors.counting()
                        )
                );
    }
 
    public List<Date> getNegativeBalanceDates() {
        List<Date> negativeBalanceDates = new ArrayList<>();
        BigDecimal balance = BigDecimal.ZERO;
 
        for (Springtaskentity springtaskentity : springtaskentity) {
            if (springtaskentity. getTransactiontype == TransactionType.Credit) {
                balance = balance.add(((Springtaskentity) springtaskentity).getAmount());
            } else {
                balance = balance.subtract(((Springtaskentity) springtaskentity).getAmount());
            }
 
            if (balance.compareTo(BigDecimal.ZERO) < 0) {
                negativeBalanceDates.add(((springtaskentity) ).getDate());
            }
        }
 
        return negativeBalanceDates;
    }
}
